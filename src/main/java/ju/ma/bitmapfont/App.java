/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ju.ma.bitmapfont;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GraphicsEnvironment;
import java.awt.event.ItemEvent;
import javax.swing.ImageIcon;
import javax.swing.JTable;
import javax.swing.UIDefaults;
import javax.swing.UIManager;
import javax.swing.table.DefaultTableCellRenderer;
import ju.ma.graphic.CharGraphic;
import org.apache.log4j.BasicConfigurator;
import org.fife.ui.rsyntaxtextarea.RSyntaxTextArea;
import org.fife.ui.rsyntaxtextarea.SyntaxConstants;
import org.fife.ui.rtextarea.RTextScrollPane;

class CustomRenderer extends DefaultTableCellRenderer {

    private static final long serialVersionUID = 6703872492730589499L;

    @Override
    public Component getTableCellRendererComponent(JTable table, Object value,
            boolean isSelected, boolean hasFocus, int row, int column) {
        Component cellComponent = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
        if (value.toString().equals("1")) {
            cellComponent.setBackground(Color.BLACK);
            cellComponent.setForeground(Color.BLACK);
        } else if (value.toString().equals("0")) {
            cellComponent.setBackground(Color.WHITE);
            cellComponent.setForeground(Color.WHITE);
        }

        return cellComponent;
    }
}

/**
 *
 * @author stevej
 */
public class App extends javax.swing.JFrame {

    private RSyntaxTextArea textArea;
    private CharGraphic CurrentCharacter;

    /**
     * Creates new form App
     */
    public App() {
        initComponents();
        updateCharacterMap();
        updateFontMap();

        Font.setSelectedItem("Comic Sans MS");
        FontSize.setSelectedItem("12");

        textArea = new RSyntaxTextArea(20, 60);
        textArea.setSyntaxEditingStyle(SyntaxConstants.SYNTAX_STYLE_C);
        textArea.setCodeFoldingEnabled(true);
        textArea.setAntiAliasingEnabled(true);
        RTextScrollPane sp = new RTextScrollPane(textArea);
        sp.setFoldIndicatorEnabled(true);
        FontOptPane.add("Code", sp);

        updateFontPreview();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        TabPane = new javax.swing.JTabbedPane();
        FontsPane = new javax.swing.JPanel();
        Font = new javax.swing.JComboBox();
        FontOptPane = new javax.swing.JTabbedPane();
        CharacterPane = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        CharMap = new javax.swing.JTextArea();
        CharPreviewPane = new javax.swing.JPanel();
        CharSelector = new javax.swing.JComboBox();
        jLabel4 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        FontTable = new javax.swing.JTable();
        CharWidth = new javax.swing.JTextField();
        CharHeight = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        Tx = new javax.swing.JSpinner();
        Ty = new javax.swing.JSpinner();
        previewLabel = new javax.swing.JLabel();
        Generate = new javax.swing.JButton();
        PrevButton = new javax.swing.JButton();
        NextButton = new javax.swing.JButton();
        FontSize = new javax.swing.JComboBox();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("BitMap Font Generator");

        Font.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        Font.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                FontItemStateChanged(evt);
            }
        });

        FontOptPane.setTabPlacement(javax.swing.JTabbedPane.LEFT);

        CharMap.setColumns(20);
        CharMap.setLineWrap(true);
        CharMap.setRows(5);
        CharMap.setText("abcdefghijklmnopqrstuvwxyz ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!\"£$%^&*()-=_+[]{};:'@#~\\|,<.>/?`¬");
        jScrollPane2.setViewportView(CharMap);

        javax.swing.GroupLayout CharacterPaneLayout = new javax.swing.GroupLayout(CharacterPane);
        CharacterPane.setLayout(CharacterPaneLayout);
        CharacterPaneLayout.setHorizontalGroup(
            CharacterPaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(CharacterPaneLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 452, Short.MAX_VALUE)
                .addContainerGap())
        );
        CharacterPaneLayout.setVerticalGroup(
            CharacterPaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(CharacterPaneLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 318, Short.MAX_VALUE)
                .addContainerGap())
        );

        FontOptPane.addTab("Characters", CharacterPane);

        CharPreviewPane.addComponentListener(new java.awt.event.ComponentAdapter() {
            public void componentShown(java.awt.event.ComponentEvent evt) {
                CharPreviewPaneComponentShown(evt);
            }
        });

        CharSelector.setEditable(true);
        CharSelector.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        CharSelector.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                CharSelectorItemStateChanged(evt);
            }
        });

        jLabel4.setText("Character");

        FontTable.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        FontTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        FontTable.setFocusable(false);
        FontTable.setRowSelectionAllowed(false);
        FontTable.setTableHeader(null);
        jScrollPane1.setViewportView(FontTable);

        CharWidth.setEditable(false);

        CharHeight.setEditable(false);

        jLabel3.setText("H:");

        jLabel5.setText("W:");

        jLabel1.setText("Tx");

        jLabel2.setText("Ty");

        Tx.setModel(new javax.swing.SpinnerNumberModel(Integer.valueOf(0), Integer.valueOf(0), null, Integer.valueOf(1)));
        Tx.setToolTipText("Number of rows to trim from the top");
        Tx.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                TxStateChanged(evt);
            }
        });

        Ty.setModel(new javax.swing.SpinnerNumberModel(Integer.valueOf(0), Integer.valueOf(0), null, Integer.valueOf(1)));
        Ty.setToolTipText("Number of rows to trim from the bottom");
        Ty.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                TyStateChanged(evt);
            }
        });

        previewLabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        previewLabel.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        Generate.setText("Generate Code");
        Generate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                GenerateActionPerformed(evt);
            }
        });

        PrevButton.setText("<");
        PrevButton.setToolTipText("Previous character");
        PrevButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                PrevButtonActionPerformed(evt);
            }
        });

        NextButton.setText(">");
        NextButton.setToolTipText("Next character");
        NextButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                NextButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout CharPreviewPaneLayout = new javax.swing.GroupLayout(CharPreviewPane);
        CharPreviewPane.setLayout(CharPreviewPaneLayout);
        CharPreviewPaneLayout.setHorizontalGroup(
            CharPreviewPaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(CharPreviewPaneLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(CharPreviewPaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1)
                    .addGroup(CharPreviewPaneLayout.createSequentialGroup()
                        .addComponent(jLabel4)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(CharSelector, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel5)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(CharWidth, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel3)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(CharHeight, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(38, 38, 38))
                    .addGroup(CharPreviewPaneLayout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(Tx, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(Ty, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(previewLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(Generate)
                        .addGap(18, 18, 18)
                        .addComponent(PrevButton)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(NextButton)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        CharPreviewPaneLayout.setVerticalGroup(
            CharPreviewPaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(CharPreviewPaneLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(CharPreviewPaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(CharSelector, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(CharPreviewPaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel4)
                        .addComponent(jLabel5)
                        .addComponent(CharWidth, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel3)
                        .addComponent(CharHeight, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 258, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(CharPreviewPaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(CharPreviewPaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                        .addGroup(CharPreviewPaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel1)
                            .addComponent(Tx, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel2)
                            .addComponent(Ty, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(CharPreviewPaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(Generate)
                            .addComponent(PrevButton)
                            .addComponent(NextButton)))
                    .addComponent(previewLabel, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        FontOptPane.addTab("Preview", CharPreviewPane);

        FontSize.setEditable(true);
        FontSize.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "8","9","10","11","12", "13","14","15","16","17","18","19", "20","21","22","23","24","25","26","28","30", "34","36","48", "72"}));
        FontSize.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                FontSizeItemStateChanged(evt);
            }
        });

        javax.swing.GroupLayout FontsPaneLayout = new javax.swing.GroupLayout(FontsPane);
        FontsPane.setLayout(FontsPaneLayout);
        FontsPaneLayout.setHorizontalGroup(
            FontsPaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(FontsPaneLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(FontsPaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(FontOptPane)
                    .addGroup(FontsPaneLayout.createSequentialGroup()
                        .addGap(4, 4, 4)
                        .addComponent(Font, javax.swing.GroupLayout.PREFERRED_SIZE, 276, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(FontSize, javax.swing.GroupLayout.PREFERRED_SIZE, 84, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        FontsPaneLayout.setVerticalGroup(
            FontsPaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(FontsPaneLayout.createSequentialGroup()
                .addGroup(FontsPaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(Font, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(FontSize, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(FontOptPane)
                .addContainerGap())
        );

        TabPane.addTab("Fonts", FontsPane);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(TabPane)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(TabPane)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void FontItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_FontItemStateChanged
        if (evt.getStateChange() == ItemEvent.SELECTED) {
            updateFontPreview();
        }
    }//GEN-LAST:event_FontItemStateChanged

    private void FontSizeItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_FontSizeItemStateChanged
        if (evt.getStateChange() == ItemEvent.SELECTED) {
            Tx.setValue(0);
            Ty.setValue(0);
            updateFontPreview();
        }
    }//GEN-LAST:event_FontSizeItemStateChanged

    private void CharPreviewPaneComponentShown(java.awt.event.ComponentEvent evt) {//GEN-FIRST:event_CharPreviewPaneComponentShown
        updateCharacterMap();
        updateFontPreview();
    }//GEN-LAST:event_CharPreviewPaneComponentShown

    private void CharSelectorItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_CharSelectorItemStateChanged
        if (evt.getStateChange() == ItemEvent.SELECTED) {
            updateFontPreview();
        }
    }//GEN-LAST:event_CharSelectorItemStateChanged

    private void TxStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_TxStateChanged
        CurrentCharacter.setTrimTop(Integer.parseInt(Tx.getValue().toString()));
        redrawFont();
    }//GEN-LAST:event_TxStateChanged

    private void TyStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_TyStateChanged
        CurrentCharacter.setTrimBottom(Integer.parseInt(Ty.getValue().toString()));
        redrawFont();
    }//GEN-LAST:event_TyStateChanged

    private void GenerateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_GenerateActionPerformed
        textArea.setText(getSourceCode());
        FontOptPane.setSelectedIndex(2);
    }//GEN-LAST:event_GenerateActionPerformed

    private void PrevButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_PrevButtonActionPerformed
        int i = CharSelector.getSelectedIndex();
        try {
            if(i > 0) {
                CharSelector.setSelectedIndex(i-1);
            }else {
                CharSelector.setSelectedIndex(CharSelector.getItemCount()-1);
            }
        }catch(Exception e) {
            
        }
    }//GEN-LAST:event_PrevButtonActionPerformed

    private void NextButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_NextButtonActionPerformed
        int i = CharSelector.getSelectedIndex();
        try {
             CharSelector.setSelectedIndex(i+1);            
        }catch(Exception e) {
            CharSelector.setSelectedIndex(0); 
        }
    }//GEN-LAST:event_NextButtonActionPerformed

    private void updateFontMap() {
        GraphicsEnvironment e = GraphicsEnvironment.getLocalGraphicsEnvironment();
        Font[] fonts = e.getAllFonts();
        String[] data = new String[fonts.length];
        int i = 0;
        for (Font f : fonts) {
            data[i] = f.getFontName();
            i++;
        }

        Font.setModel(new javax.swing.DefaultComboBoxModel(data));
    }

    private void paintTableRow(Object[][] data) {
        for (int row = 0; row < data.length; row++) {
            for (int col = 0; col < data[row].length; col++) {
                int val = Integer.valueOf(data[row][col].toString());
                FontTable.setValueAt(val, row, col);
            }
        }
    }

    private void updateTableGrid(int width, int height) {
        try {
            Object[][] data = new Object[height][width];
            for (int i = 0; i < height; i++) {
                for (int j = 0; j < width; j++) {
                    data[i][j] = "0";
                }
            }

            FontTable.setSize(width, height);
            FontTable.setMinimumSize(new Dimension(width, height));
            FontTable.setModel(new javax.swing.table.DefaultTableModel(
                    data,
                    data[0]));
            for (int i = 0; i < width; i++) {
                FontTable.getColumnModel().getColumn(i).setCellRenderer(
                        new CustomRenderer());
            }
        } catch (Exception e) {
        }
    }

    private void updateCharacterMap() {
        String data = CharMap.getText();
        int val = CharSelector.getSelectedIndex();
        if (data != null && data.trim().length() > 0) {
            char[] chars = data.trim().toCharArray();
            String[] data_char = new String[chars.length];
            for (int i = 0; i < chars.length; i++) {
                data_char[i] = String.valueOf(chars[i]);
            }

            CharSelector.setModel(
                    new javax.swing.DefaultComboBoxModel(data_char));
            CharSelector.setSelectedIndex(val);
        }
    }

    private String getSourceCode() {
        String data = CharMap.getText();
        StringBuilder build = new StringBuilder();

        StringBuilder FontArray = new StringBuilder();
        StringBuilder OffsetArray = new StringBuilder();
        StringBuilder WidthArray = new StringBuilder();
        StringBuilder SizeArray = new StringBuilder();
        StringBuilder HeightArray = new StringBuilder();

        int height = -1;
        int offset = 0;
        boolean diff_height = false;

        if (data != null && data.trim().length() > 0) {
            char[] chars = data.trim().toCharArray();
            for (int i = 0; i < chars.length; i++) {
                String c = String.valueOf(chars[i]);
                CharGraphic char_g = new CharGraphic(
                        c,
                        Font.getSelectedItem().toString(),
                        Integer.parseInt(FontSize.getSelectedItem().toString()),
                        Integer.parseInt(Tx.getValue().toString()),
                        Integer.parseInt(Ty.getValue().toString()));
                char_g.setOffset(offset);

                FontArray.append(char_g.getBytes()).append("\n");
                String hex_char = CharGraphic.hex((int)chars[i]);

                OffsetArray.append(String.format("\t\t\tcase %s: theOffset = %d; break; /* %s */\n",
                        hex_char, offset, c));
                WidthArray.append(String.format("\t\t\tcase %s: theWidth = %d; break; /* %s */\n",
                        hex_char, char_g.getWidth(), c));
                SizeArray.append(String.format("\t\t\tcase %s: theSize = %d; break; /* %s */\n",
                        hex_char, char_g.getSize(), c));
                HeightArray.append(String.format("\t\t\tcase %s: theOffset = %d; break; /* %s */\n",
                        hex_char, char_g.getHeight(), c));
                if (height == -1) {
                    height = char_g.getHeight();
                }

                if (height != char_g.getHeight()) {
                    diff_height = true;
                }

                offset += char_g.getSize();
            }
        }

        String dt = FontArray.toString().trim();
        build.append(String.format("/* Size of font data is %d bytes*/\n"
                + "rom unsigned char theFont[%d] = \n"
                + "{\n"
                + "\t%s\n"
                + "}\n\n", offset, offset, dt.substring(0, dt.length() - 1)));

        build.append(String.format("/* \n"
                + " * Method to get the location of a character.\n"
                + " * @param theIndex, the ASCII code for the character\n"
                + " * @return int the Offset of the character.\n"
                + " **/ \n"
                + "int getCharOffset(char theIndex)\n"
                + "{\n"
                + "	    int theOffset = 0;\n"
                + "	    \n"
                + "	    switch(theIndex)\n"
                + "	    {\n"
                + "%s\n"
                + "	    }\n"
                + "	    \n"
                + "	    return theOffset;\n"
                + "}\n\n", OffsetArray.toString()));

        build.append(String.format("/* \n"
                + " * Method to Get the width of a character, usefull when writting strings.\n"
                + " * @param theIndex, the ASCII code for the character\n"
                + " * @return char the width of the character.\n"
                + " **/ \n"
                + "char getCharWidth(char theIndex)\n"
                + "{\n"
                + "		  int theWidth = 0;\n"
                + "		  \n"
                + "		  switch(theIndex)\n"
                + "		  {\n"
                + "%s\n"
                + "		  }\n"
                + "\n"
                + "		  return theWidth;\n"
                + "}\n\n", WidthArray.toString()));

        build.append(String.format("/* \n"
                + " * Method to get the size of a given character. \n"
                + " * @param theIndex ASCII code for the character\n"
                + " * @return char the size of the character.\n"
                + " **/ \n"
                + "char getCharSize(char theIndex)\n"
                + "{\n"
                + "		 char theSize = 0;\n"
                + "		 \n"
                + "		 switch(theIndex)\n"
                + "		 {\n"
                + "%s\n"
                + "		 }\n"
                + "\n"
                + "		 return theSize;\n"
                + "}\n\n", SizeArray.toString()));
        
        if(diff_height) {
            build.append(String.format("/* \n"
                + " * Method to get the height of a given character. \n"
                + " * @param theIndex ASCII code for the character\n"
                + " * @return char the size of the character.\n"
                + " **/ \n"
                + "char getCharHeight(char theIndex)\n"
                + "{\n"
                + "		 char theHeight = 0;\n"
                + "		 \n"
                + "		 switch(theIndex)\n"
                + "		 {\n"
                + "%s\n"
                + "		 }\n"
                + "\n"
                + "		 return theHeight;\n"
                + "}\n\n", HeightArray.toString()));
        }else {
             build.append(String.format("/* \n"
                + " * Method to get the height of a given character. \n"
                + " * @param theIndex ASCII code for the character\n"
                + " * @return char the size of the character.\n"
                + " **/ \n"
                + "char getCharHeight(char theIndex)\n"
                + "{\n"
                + "	return %d;"
                + "\n"
                + "}\n\n", height));
        }
        
        return build.toString();
    }

    private void redrawFont() {
        updateTableGrid(CurrentCharacter.getWidth(), CurrentCharacter.getHeight());
        CharWidth.setText(String.valueOf(CurrentCharacter.getWidth()));
        CharHeight.setText(String.valueOf(CurrentCharacter.getHeight()));
        paintTableRow(CurrentCharacter.getGrid());


        StringBuilder build = new StringBuilder();
        build.append("rom unsigned char aFont[")
                .append(CurrentCharacter.getSize()).append("] = [\r\n");
        String dt = CurrentCharacter.getBytes().trim();
        build.append("\t").append(dt.substring(0, dt.length() - 1));
        build.append("\r\n];\r\n");

        textArea.setText(build.toString());
    }

    private void updateFontPreview() {
        try {

            CurrentCharacter = new CharGraphic(
                    CharSelector.getSelectedItem().toString(),
                    Font.getSelectedItem().toString(),
                    Integer.parseInt(FontSize.getSelectedItem().toString()),
                    Integer.parseInt(Tx.getValue().toString()),
                    Integer.parseInt(Ty.getValue().toString()));
            previewLabel.setIcon(new ImageIcon(CurrentCharacter.getImage()));
            redrawFont();
        } catch (Exception e) {
        }
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    UIDefaults defaults = UIManager.getLookAndFeelDefaults();
                    defaults.put("Table.gridColor", new Color(214, 217, 223));
                    defaults.put("Table.disabled", false);
                    defaults.put("Table.showGrid", true);
                    defaults.put("Table.intercellSpacing", new Dimension(1, 1));
                    break;
                }
            }
            UIManager.setLookAndFeel(
                    UIManager.getSystemLookAndFeelClassName());
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(App.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(App.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(App.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(App.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        BasicConfigurator.configure();
        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new App().setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextField CharHeight;
    private javax.swing.JTextArea CharMap;
    private javax.swing.JPanel CharPreviewPane;
    private javax.swing.JComboBox CharSelector;
    private javax.swing.JTextField CharWidth;
    private javax.swing.JPanel CharacterPane;
    private javax.swing.JComboBox Font;
    private javax.swing.JTabbedPane FontOptPane;
    private javax.swing.JComboBox FontSize;
    private javax.swing.JTable FontTable;
    private javax.swing.JPanel FontsPane;
    private javax.swing.JButton Generate;
    private javax.swing.JButton NextButton;
    private javax.swing.JButton PrevButton;
    private javax.swing.JTabbedPane TabPane;
    private javax.swing.JSpinner Tx;
    private javax.swing.JSpinner Ty;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JLabel previewLabel;
    // End of variables declaration//GEN-END:variables
}
