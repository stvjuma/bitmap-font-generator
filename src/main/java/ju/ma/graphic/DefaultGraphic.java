/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ju.ma.graphic;

/**
 *
 * @author stevej
 */
public interface DefaultGraphic {
    /**
     * Returns the width of the graphic
     * 
     * @return 
     */
    public int getWidth();
    
    /**
     * Returns the Height of the graphic
     * 
     * @return 
     */
    public int getHeight();
    
    /**
     * Returns the size in bytes of the Graphic element
     * 
     * @return 
     */
    public int getSize();
    
    /**
     * Returns the Offset in the global array for the Graphic element
     * 
     * @return 
     */
    public int getOffset();
    
    /**
     * Sets the offset in the global array for this graphic element
     * 
     * @param offset The offset for this graphic element
     */
    public void setOffset(int offset);
    
    /**
     * Returns a string representation of the bytes that make up this graphic
     * element
     * 
     * @return 
     */
    public String getBytes();
    
    /**
     * Returns The character map grid that represents this graphic
     * 
     * @return 
     */
    public Object[][] getGrid();
}
