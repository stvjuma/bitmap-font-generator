/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ju.ma.graphic;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import org.apache.log4j.Logger;

/**
 *
 * @author stevej
 */
public class CharGraphic implements DefaultGraphic {
    private static Logger logger = Logger.getLogger(CharGraphic.class);
    private Object[][] grid;
    BufferedImage charImage;
    private String character = " ";
    private int offset = 0;
    private int trimTop = 0;
    private int trimBottom = 0;
    
    public void setTrimTop(int trim) {
        if(trim < 0) return;
        trimTop = trim;
        generateGrid(charImage);
    }
    
    public void setTrimBottom(int trim) {
        if(trim < 0) return;
        trimBottom = trim;
        generateGrid(charImage);
    }
    
    public CharGraphic(String character, String fnt, int size) {
        Font font = new Font(fnt, Font.PLAIN, size);
        charImage = getCharImage(character.toCharArray()[0], font, size);
        generateGrid(charImage);
        this.character = character;
        logger.info(String.format("\n"
                + "Char   \t:\t %s \r\n"
                + "Width  \t:\t %d \r\n"
                + "Height \t:\t %d \r\n", character, grid[0].length, 
                grid.length));
    }
    
    public CharGraphic(String character, String fnt, int size, int trimTop,
            int trimBottom) {
        this.trimTop = trimTop;
        this.trimBottom = trimBottom;
        Font font = new Font(fnt, Font.PLAIN, size);
        charImage = getCharImage(character.toCharArray()[0], font, size);
        generateGrid(charImage);
        this.character = character;
        logger.info(String.format("\n"
                + "Char   \t:\t %s \r\n"
                + "Width  \t:\t %d \r\n"
                + "Height \t:\t %d \r\n", character, grid[0].length, 
                grid.length));
    }

    private BufferedImage getCharImage(char ch, Font font, int size) {
        BufferedImage sizeImage = new BufferedImage(1, 1, BufferedImage.TYPE_INT_ARGB);
        Graphics2D sizeGraphics = (Graphics2D) sizeImage.getGraphics();

        sizeGraphics.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);


        FontMetrics fontMetrics = sizeGraphics.getFontMetrics(font);
        int charwidth = fontMetrics.charWidth(ch);
        if (charwidth <= 0) {
            charwidth = 1;
        }

        int charheight = fontMetrics.getHeight();
        if (charheight <= 0) {
            charheight = size;
        }

        BufferedImage chrImage = new BufferedImage(charwidth, charheight, BufferedImage.TYPE_INT_ARGB);
        Graphics2D charGraphics = (Graphics2D) chrImage.getGraphics();


        charGraphics.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);


        charGraphics.setFont(font);
        charGraphics.setBackground(Color.WHITE);
        charGraphics.setColor(Color.BLACK);
        charGraphics.drawString(String.valueOf(ch), 0, fontMetrics.getAscent());

        return chrImage;
    }

    private void generateGrid(BufferedImage image) {
        grid = new Object[image.getHeight()-(trimTop+trimBottom)][image.getWidth()];
        StringBuilder build = new StringBuilder();
        for (int y = trimTop; y < image.getHeight()-trimBottom; y++) {
            for (int x = 0; x < image.getWidth(); x++) {
                grid[y-trimTop][x] = image.getRGB(x, y);
                if (image.getRGB(x, y) == 0) {
                    grid[y-trimTop][x] = 0;
                    build.append("0");
                } else {
                    grid[y-trimTop][x] = 1;
                    build.append(" ");
                }
            }
            
            build.append("\r\n");
        }
        
        logger.info("ASCII \n"+build.toString());
    }

    public BufferedImage getImage(){
        return charImage;
    }
    
    public int getWidth() {
       return grid[0].length;
    }

    public int getHeight() {
        return grid.length;
    }

    public Object[][] getGrid() {
        return grid;
    }
    
    /**
     * Converts a binary string to hex
     * 
     * @param binary
     * @return 
     */
    public static String hex(String binary) {
        int val = Integer.valueOf(binary, 2);
        return hex(val);
    }
    
    public static String hex(int val) {
        String val_hex = Integer.toHexString(val);
        if(val_hex.length() < 2) {
            val_hex = "0"+val_hex;
        }
        
        return "0x"+val_hex.toUpperCase();
    }
    
    /**
     * Returns the size in bytes for the character array
     * 
     * @return 
     */
    public int getSize() {
        if(getWidth() > 8) {
            double size =  Math.ceil((double)getWidth()/(double)8)*(double)getHeight();  
            return (int)size;
        }else {
            return grid.length;
        }
    }
    
    public int getOffset() {
        return offset;
    }
    
    public String getBytes() {
        StringBuilder out = new StringBuilder();
        out.append("\t/* character ")
                .append(hex((int)character.toCharArray()[0]))
                .append(" ('")
                .append(character)
                .append("'):\r\n\t * (width=")
                .append(getWidth())
                .append(", height=")
                .append(getHeight())
                .append(",size=")
                .append(getSize())
                .append(" Bytes Offset=")
                .append(getOffset()).append(") */\r\n\t");
        int size = 0;
        for(int row=0; row<grid.length; row++) {
            StringBuilder build = new StringBuilder();
            for(int col=0; col<grid[row].length; col++) {
                build.append(String.valueOf(grid[row][col]));
                if(build.length() == 8) {
                    out.append(hex(build.toString())).append(",");
                    build = new StringBuilder();
                    size++;
                    if(size == 8){
                        out.append("\r\n\t");
                        size = 0;
                    }
                }
            }
            
            if (build.length() > 0) {
                out.append(hex(build.toString())).append(",");
                size++;
                if(size == 8){
                    out.append("\r\n\t");
                    size = 0;
                }
            }
            
        }
        
        if(size != 0) {
            out.append("\r\n");
        }
        
        return out.toString();
    }

    public void setOffset(int offset) {
        this.offset = offset;
    }
}
